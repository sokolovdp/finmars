FROM python:3.11-alpine

ENV PYTHONUNBUFFERED 1

COPY . /finmars

WORKDIR /finmars
EXPOSE 8000

RUN apk add --update --no-cache postgresql-client
RUN apk add --update --no-cache --virtual .tmp-deps build-base postgresql-dev musl-dev
RUN apk del .tmp-deps
RUN python -m venv /venv && /venv/bin/pip install --upgrade pip
RUN /venv/bin/pip install --no-cache-dir -r dev_requirements.txt
RUN adduser --disabled-password --no-create-home finmars
RUN chown -R finmars:finmars /finmars && chmod -R 755 /finmars && chmod -R +x /finmars

ENV PATH="/finmars:/venv/bin:$PATH"

USER finmars
