from django.core.serializers.json import DjangoJSONEncoder
from django.db import models


class ExtraFieldsMixin(models.Model):
    extra_fields = models.JSONField(
        encoder=DjangoJSONEncoder,
        default=dict,
        help_text="user defined fields",
    )

    class Meta:
        abstract = True
