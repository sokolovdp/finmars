from django.http.request import QueryDict

DEFAULT_ORDER_PARAM = "order_by"
FILTERS_PREFIX = "filter."
EXTRA_FIELDS = "extra_fields."


class QueryFiltersExtractor:
    """
    Extract filters from query parameters of the request:
    filter. - usual filters
    extra_fields. - filter by user defined params in extra_fields json
    order_by - ordering
    """

    def __init__(self, params: QueryDict, default_order: str):
        self.params = params
        self.default_order = default_order
        self.filters = self.extract_filters()
        self.order = self.extract_order()

    def extract_order(self):
        order = self.params.get(DEFAULT_ORDER_PARAM)
        if isinstance(order, str):
            order = order.strip()
        return order or self.default_order

    def extract_filters(self):
        filters = {}
        for param_key, param_value in self.params.items():
            if param_key.startswith(FILTERS_PREFIX):
                if filter_key := param_key[len(FILTERS_PREFIX) :]:
                    filters[filter_key] = param_value
                continue

            if param_key.startswith(EXTRA_FIELDS):
                if filter_key := param_key[len(EXTRA_FIELDS) :]:
                    if param_value.isdigit():
                        param_value = int(param_value)
                    filters[f"extra_fields__{filter_key}"] = param_value
                continue

        return filters
