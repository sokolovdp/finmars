from django.conf import settings
from drf_spectacular.utils import OpenApiTypes, extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


@extend_schema_view(
    retrieve=extend_schema(
        parameters=[],
        request=None,
        responses={200: OpenApiTypes.JSON_PTR},
    ),
)
@extend_schema(tags=["Version"])
class VersionView(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    @staticmethod
    def retrieve(request, *args, **kwargs):
        response = {
            "version": settings.VERSION,
        }
        return Response(response)
