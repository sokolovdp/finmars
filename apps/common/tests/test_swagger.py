import io

from django.core.management import call_command
from django.test import TestCase


class SwaggerTestCase(TestCase):
    def test_swagger_generate(self):
        with io.StringIO() as out:
            call_command(
                "spectacular",
                validate=True,
                fail_on_warn=True,
                traceback=True,
                stdout=out,
            )
            self.assertIn("openapi", out.getvalue(), out.getvalue())
