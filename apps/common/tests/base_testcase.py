import random
import string
from datetime import date, datetime, timedelta
from decimal import Decimal

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from apps.investor.models import Investor


class TestMetaClass(type):
    def __new__(mcs, name, bases, dct):
        # sourcery skip: class-method-first-arg-name
        for attr_name in list(dct.keys()):
            if hasattr(dct[attr_name], "test_cases"):
                cases = dct[attr_name].test_cases
                del dct[attr_name].test_cases
                hidden_name = f"__{attr_name}"
                mcs._move_method(dct, attr_name, hidden_name)

                for case in cases:
                    mcs._add_test_method(dct, attr_name, hidden_name, case[0], case[1:])

        return super(TestMetaClass, mcs).__new__(mcs, name, bases, dct)

    @classmethod
    def _move_method(mcs, dct, from_name, to_name):
        # sourcery skip: class-method-first-arg-name
        dct[to_name] = dct[from_name]
        dct[to_name].__name__ = str(to_name)
        del dct[from_name]

    @classmethod
    def _add_test_method(mcs, dct, orig_name, hidden_name, postfix, params):
        test_method_name = "{}__{}".format(orig_name, postfix)

        def test_method(self):
            return getattr(self, hidden_name)(*params)

        test_method.__name__ = test_method_name
        dct[test_method_name] = test_method


class BaseTestCase(TestCase, metaclass=TestMetaClass):
    user: User
    token: Token
    investor: Investor
    client: APIClient

    @classmethod
    def cases(cls, *cases):
        """
        Create a bunch of test methods using the case table and test code.
        Example. The following two pieces of code would behave identically:

        @BaseTestCase.cases(['name1', 1], ['name2', 2])
        def test_example(self, number):
            self.assertGreater(number, 0)

        def __test_example(self, number):
            self.assertGreater(number, 0)
        def test_example__name1(self):
            return self.__test_example(1)
        def test_example__name2(self):
            return self.__test_example(2)
        """

        def decorator(test_method):
            test_method.test_cases = cases
            return test_method

        return decorator

    @classmethod
    def random_int(cls, _min: int = 1, _max: int = 10000) -> int:
        return random.randint(_min, _max)

    @classmethod
    def random_string(cls, length: int = 10) -> str:
        return "".join(
            random.SystemRandom().choice(string.ascii_uppercase) for _ in range(length)
        )

    @classmethod
    def random_decimal(cls, _min: int = 0, _max: int = 1000) -> Decimal:
        float_value = random.uniform(_min, _max)
        return Decimal(f"{round(float_value, 2):.2f}")

    @classmethod
    def random_email(cls) -> str:
        return f"{cls.random_string(5)}@{cls.random_string(3)}.{cls.random_string(2)}"

    @classmethod
    def random_future_date(cls, interval=30) -> date:
        days = cls.random_int(1, interval)
        return datetime.now().date() + timedelta(days=days)

    def create_user(self, is_superuser=False, email=None) -> User:
        if not email:
            email = self.random_email()

        user, _ = User.objects.get_or_create(
            username=self.random_string(),
            email=email,
            is_superuser=is_superuser,
        )
        return user

    def create_investor(self, user=None, name=None, vip=False) -> Investor:
        if not user:
            user = self.create_user()

        if not name:
            name = self.random_string()
        investor, _ = Investor.objects.get_or_create(user=user, name=name, vip=vip)
        return investor

    def init_test_case(self):
        self.user = self.create_user()
        self.investor = self.create_investor(user=self.user)
        self.token = Token.objects.get(user=self.user)
        self.client = APIClient(HTTP_AUTHORIZATION=f"Token {self.token.key}")
