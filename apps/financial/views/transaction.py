from drf_spectacular.utils import extend_schema

from apps.financial.models import Transaction
from apps.financial.serializers import TransactionSerializer
from apps.financial.views.base_view import AccountBasedViewSet


@extend_schema(tags=["Transaction"])
class TransactionViewSet(AccountBasedViewSet):
    """
    Transaction CRUD API
    """

    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
