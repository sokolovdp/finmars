from drf_spectacular.utils import extend_schema

from apps.financial.models import Portfolio
from apps.financial.serializers import PortfolioSerializer
from apps.financial.views.base_view import AccountBasedViewSet


@extend_schema(tags=["Portfolio"])
class PortfolioViewSet(AccountBasedViewSet):
    """
    Portfolio CRUD API
    """

    queryset = Portfolio.objects.all()
    serializer_class = PortfolioSerializer
