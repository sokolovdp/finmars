from django.db.models import QuerySet
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from apps.common.query_filters_extractor import QueryFiltersExtractor
from apps.financial.models import Account
from apps.financial.serializers import AccountSerializer
from apps.financial.settings import DEFAULT_ORDER
from apps.investor.models import Investor


@extend_schema(tags=["Account"])
class AccountViewSet(ModelViewSet):
    """
    Account CRUD API
    """

    permission_classes = [IsAuthenticated]
    pagination_class = None
    http_method_names = ["get", "post", "patch", "delete"]
    serializer_class = AccountSerializer
    queryset = Account.objects

    def get_queryset(self) -> QuerySet:
        if not (investor := Investor.objects.filter(user=self.request.user).first()):
            raise ValidationError("No such investor")

        # ensure that investor can access only his accounts
        investors_accounts = self.queryset.filter(investor=investor)

        # extract common and extra_fields filters from query params
        extractor = QueryFiltersExtractor(self.request.query_params, DEFAULT_ORDER)
        return investors_accounts.filter(**extractor.filters).order_by(extractor.order)
