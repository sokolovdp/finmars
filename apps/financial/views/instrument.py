from drf_spectacular.utils import extend_schema

from apps.financial.models import Instrument
from apps.financial.serializers import InstrumentSerializer
from apps.financial.views.base_view import AccountBasedViewSet


@extend_schema(tags=["Instrument"])
class InstrumentViewSet(AccountBasedViewSet):
    """
    Instrument CRUD API
    """

    queryset = Instrument.objects.all()
    serializer_class = InstrumentSerializer
