from drf_spectacular.utils import extend_schema

from apps.financial.models import Currency
from apps.financial.serializers import CurrencySerializer
from apps.financial.views.base_view import AccountBasedViewSet


@extend_schema(tags=["Currency"])
class CurrencyViewSet(AccountBasedViewSet):
    """
    Currency CRUD API
    """

    queryset = Currency.objects
    serializer_class = CurrencySerializer
