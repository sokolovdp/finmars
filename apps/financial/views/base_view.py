from django.db.models import QuerySet
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from apps.common.query_filters_extractor import QueryFiltersExtractor
from apps.financial.models import Account
from apps.financial.settings import DEFAULT_ORDER
from apps.investor.models import Investor


class AccountBasedViewSet(ModelViewSet):
    """
    Base ModelViewSet for Financial Account based CRUD API
    """

    permission_classes = [IsAuthenticated]
    pagination_class = None
    http_method_names = ["get", "post", "patch", "delete"]

    def get_queryset(self) -> QuerySet:
        if not (investor := Investor.objects.filter(user=self.request.user).first()):
            raise ValidationError("No such investor")

        # investor has access only to his accounts
        investor_accounts = Account.objects.filter(investor=investor)

        # extract common and extra_fields filters from query params
        extractor = QueryFiltersExtractor(self.request.query_params, DEFAULT_ORDER)
        return (
            self.queryset.filter(account__in=investor_accounts)
            .filter(**extractor.filters)
            .order_by(extractor.order)
        )
