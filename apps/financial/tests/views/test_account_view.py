from apps.common.tests.base_testcase import BaseTestCase
from apps.financial.models import Account
from apps.financial.settings import JSON_FORMAT, NAME_LENGTH, TYPE_LENGTH

WRONG_NAME = BaseTestCase.random_string(length=NAME_LENGTH + 1)
WRONG_TYPE = BaseTestCase.random_string(length=TYPE_LENGTH + 1)


# noinspection DuplicatedCode
class AccountViewSetTest(BaseTestCase):
    def setUp(self):
        self.init_test_case()
        self.url = "/api/account/"

    def create_test_account(self, _type=None, _name=None) -> dict:
        account_data = {
            "type": _type or self.random_string(),
            "name": _name or self.random_string(),
        }
        response = self.client.post(self.url, data=account_data, format=JSON_FORMAT)
        return response.json()

    @BaseTestCase.cases(
        ("no_data", {}),
        ("no_type", {"type": None, "name": "name"}),
        ("no_name", {"type": "type", "name": None}),
    )
    def test__create_missing_data_error(self, account_data):
        response = self.client.post(self.url, data=account_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    @BaseTestCase.cases(
        ("bad_name", {"type": "name", "name": WRONG_NAME}),
        ("bad_type", {"type": WRONG_TYPE, "name": "name"}),
    )
    def test__create_invalid_data_error(self, invalid_data):
        response = self.client.post(self.url, data=invalid_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    def test__create_ok(self):
        account_name = self.random_string(length=NAME_LENGTH)
        account_type = self.random_string(length=TYPE_LENGTH)
        account_data = {
            "type": account_type,
            "name": account_name,
        }
        response = self.client.post(self.url, data=account_data, format=JSON_FORMAT)
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        self.assertIn("created", resp_data)
        self.assertIn("modified", resp_data)
        self.assertEqual(resp_data["extra_fields"], {})
        self.assertEqual(resp_data["type"], account_type)
        self.assertEqual(resp_data["name"], account_name)
        self.assertNotIn("investor", resp_data)

    def test__get_list_ok(self):
        self.create_test_account()
        self.create_test_account()

        response = self.client.get(self.url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__put_method_is_not_allowed(self):
        account = self.create_test_account()
        response = self.client.put(
            f"{self.url}{account['id']}/",
            data=account,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 405)

    @BaseTestCase.cases(
        ("new_name", {"name": "new_name"}),
        ("new_type", {"type": "new_type"}),
    )
    def test__patch_ok(self, patch_data):
        account = self.create_test_account()
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f"{self.url}{account['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        field = list(patch_data)[0]
        self.assertIn("new_", resp_data[field])

    def test__get_retrieve_ok(self):
        account = self.create_test_account()
        response = self.client.get(
            f"{self.url}{account['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()

        self.assertEqual(resp_data["type"], account["type"])
        self.assertEqual(resp_data["name"], account["name"])
        self.assertEqual(resp_data["id"], account["id"])
        self.assertNotIn("investor", resp_data)

    def test__delete_ok(self):
        account = self.create_test_account()
        response = self.client.delete(
            f"{self.url}{account['id']}/",
            data=None,
        )
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(Account.objects.filter(id=account["id"]).first())

    def test__change_investor_is_not_allowed(self):
        account = self.create_test_account()
        account_obj_before = Account.objects.get(id=account["id"])
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data={"investor": self.random_int(1000, 10000)},
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        account_obj_after = Account.objects.get(id=account["id"])
        self.assertEqual(
            account_obj_before.investor,
            account_obj_after.investor,
        )

    def test__list_with_name_filter(self):
        account_1 = self.create_test_account()
        account_2 = self.create_test_account()

        for account in (account_1, account_2):
            url = f"{self.url}?filter.name={account['name']}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(len(resp_data), 1)
            self.assertEqual(resp_data[0]["name"], account["name"])

    def test__list_with_type_filter(self):
        _type = self.random_string()
        _ = self.create_test_account(_type=_type)
        _ = self.create_test_account(_type=_type)

        url = f"{self.url}?filter.type={_type}"
        response = self.client.get(url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__patch_extra_fields(self):
        account = self.create_test_account()
        number = self.random_int(1000, 10000)
        string = self.random_string()
        future_date = str(BaseTestCase.random_future_date())
        patch_data = {
            "extra_fields": {
                "number": number,
                "string": string,
                "date": future_date,
            }
        }
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(resp_data["extra_fields"]["number"], number)
        self.assertEqual(resp_data["extra_fields"]["string"], string)
        self.assertEqual(resp_data["extra_fields"]["date"], future_date)

    def test__list_filter_by_extra_fields(self):
        account = self.create_test_account()
        extra_fields = dict(
            number=self.random_int(1000, 10000),
            string=self.random_string(),
            date=str(BaseTestCase.random_future_date()),
        )
        patch_data = {"extra_fields": extra_fields}
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        for field, value in extra_fields.items():
            url = f"{self.url}?extra_fields.{field}={value}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(resp_data[0]["extra_fields"][field], value)
