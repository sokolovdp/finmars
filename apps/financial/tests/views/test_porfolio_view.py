from apps.common.tests.base_testcase import BaseTestCase
from apps.financial.models import Portfolio
from apps.financial.settings import JSON_FORMAT, NAME_LENGTH
from apps.financial.tests.factories import AccountFactory

WRONG_NAME = BaseTestCase.random_string(length=NAME_LENGTH + 1)


# noinspection DuplicatedCode
class PortfolioViewSetTest(BaseTestCase):
    def setUp(self):
        self.init_test_case()
        self.url = "/api/portfolio/"
        self.account = AccountFactory(
            name=self.random_string(),
            type=self.random_string(),
            investor=self.investor,
        )

    def create_test_portfolio(self, name=None, account=None) -> dict:
        portfolio_data = {
            "name": name or self.random_string(),
            "account": account or self.account.id,
        }
        response = self.client.post(self.url, data=portfolio_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 201)
        return response.json()

    @BaseTestCase.cases(
        ("no_data", {}),
        ("no_code", {"code": None, "name": "name"}),
        ("no_name", {"code": "code", "name": None}),
    )
    def test__create_missing_data_error(self, portfolio_data):
        response = self.client.post(self.url, data=portfolio_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    @BaseTestCase.cases(
        ("bad_name", {"notes": "notes", "name": WRONG_NAME}),
    )
    def test__create_invalid_data_error(self, invalid_data):
        response = self.client.post(self.url, data=invalid_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    def test__create_ok(self):
        portfolio_notes = self.random_string()
        portfolio_strategy = self.random_string()
        portfolio_name = self.random_string(length=NAME_LENGTH)
        portfolio_data = {
            "notes": portfolio_notes,
            "name": portfolio_name,
            "strategy": portfolio_strategy,
            "account": self.account.id,
        }
        response = self.client.post(self.url, data=portfolio_data, format=JSON_FORMAT)
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        self.assertIn("created", resp_data)
        self.assertIn("modified", resp_data)
        self.assertEqual(resp_data["extra_fields"], {})
        self.assertEqual(resp_data["notes"], portfolio_notes)
        self.assertEqual(resp_data["strategy"], portfolio_strategy)
        self.assertEqual(resp_data["name"], portfolio_name)
        self.assertEqual(resp_data["account"], self.account.id)

    def test__get_list_ok(self):
        self.create_test_portfolio()
        self.create_test_portfolio()

        response = self.client.get(self.url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__put_method_is_not_allowed(self):
        portfolio = self.create_test_portfolio()
        response = self.client.put(
            f"{self.url}{portfolio['id']}/",
            data=portfolio,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 405)

    @BaseTestCase.cases(
        ("new_name", {"name": "new_name"}),
        ("new_notes", {"notes": "new_notes"}),
        ("new_strategy", {"notes": "new_strategy"}),
    )
    def test__patch_ok(self, patch_data):
        portfolio = self.create_test_portfolio()
        response = self.client.patch(
            f"{self.url}{portfolio['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)

        response = self.client.get(
            f"{self.url}{portfolio['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        field = list(patch_data)[0]
        self.assertIn("new_", resp_data[field])

    def test__get_retrieve_ok(self):
        portfolio = self.create_test_portfolio()
        response = self.client.get(
            f"{self.url}{portfolio['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()

        self.assertEqual(resp_data["notes"], portfolio["notes"])
        self.assertEqual(resp_data["strategy"], portfolio["strategy"])
        self.assertEqual(resp_data["name"], portfolio["name"])
        self.assertEqual(resp_data["id"], portfolio["id"])

    def test__delete_ok(self):
        portfolio = self.create_test_portfolio()
        response = self.client.delete(
            f"{self.url}{portfolio['id']}/",
            data=None,
        )
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(Portfolio.objects.filter(id=portfolio["id"]).first())

    def test__list_with_unique_name_filter(self):
        portfolio_1 = self.create_test_portfolio()
        portfolio_2 = self.create_test_portfolio()

        for portfolio in (portfolio_1, portfolio_2):
            url = f"{self.url}?filter.name={portfolio['name']}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(len(resp_data), 1)
            self.assertEqual(resp_data[0]["name"], portfolio["name"])

    def test__list_with_common_name_filter(self):
        name = self.random_string(length=NAME_LENGTH)
        _ = self.create_test_portfolio(name=name)
        _ = self.create_test_portfolio(name=name)

        url = f"{self.url}?filter.name={name}"
        response = self.client.get(url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__patch_extra_fields(self):
        account = self.create_test_portfolio()
        number = self.random_int(1000, 10000)
        string = self.random_string()
        future_date = str(BaseTestCase.random_future_date())
        patch_data = {
            "extra_fields": {
                "number": number,
                "string": string,
                "date": future_date,
            }
        }
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(resp_data["extra_fields"]["number"], number)
        self.assertEqual(resp_data["extra_fields"]["string"], string)
        self.assertEqual(resp_data["extra_fields"]["date"], future_date)

    def test__list_filter_by_extra_fields(self):
        account = self.create_test_portfolio()
        extra_fields = dict(
            number=self.random_int(1000, 10000),
            string=self.random_string(),
            date=str(BaseTestCase.random_future_date()),
        )
        patch_data = {"extra_fields": extra_fields}
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        for field, value in extra_fields.items():
            url = f"{self.url}?extra_fields.{field}={value}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(resp_data[0]["extra_fields"][field], value)
