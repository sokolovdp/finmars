from apps.common.tests.base_testcase import BaseTestCase
from apps.financial.models import Currency
from apps.financial.settings import CODE_LENGTH, JSON_FORMAT, NAME_LENGTH
from apps.financial.tests.factories.account import AccountFactory

WRONG_NAME = BaseTestCase.random_string(length=NAME_LENGTH + 1)
WRONG_CODE = BaseTestCase.random_string(length=CODE_LENGTH + 1)


# noinspection DuplicatedCode
class CurrencyViewSetTest(BaseTestCase):
    def setUp(self):
        self.init_test_case()
        self.url = "/api/currency/"
        self.account = AccountFactory(
            name=self.random_string(),
            type=self.random_string(),
            investor=self.investor,
        )

    def create_test_currency(self, code=None, name=None, account=None) -> dict:
        currency_data = {
            "code": code or self.random_string(length=CODE_LENGTH),
            "name": name or self.random_string(),
            "account": account or self.account.id,
        }
        response = self.client.post(self.url, data=currency_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 201)
        return response.json()

    @BaseTestCase.cases(
        ("no_data", {}),
        ("no_code", {"code": None, "name": "name"}),
        ("no_name", {"code": "code", "name": None}),
    )
    def test__create_missing_data_error(self, currency_data):
        response = self.client.post(self.url, data=currency_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    @BaseTestCase.cases(
        ("bad_name", {"code": "code", "name": WRONG_CODE}),
        ("bad_code", {"code": WRONG_CODE, "name": "name"}),
    )
    def test__create_invalid_data_error(self, invalid_data):
        response = self.client.post(self.url, data=invalid_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    def test__create_ok(self):
        currency_code = self.random_string(length=CODE_LENGTH)
        currency_name = self.random_string(length=NAME_LENGTH)
        currency_data = {
            "code": currency_code,
            "name": currency_name,
            "account": self.account.id,
        }
        response = self.client.post(self.url, data=currency_data, format=JSON_FORMAT)
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        self.assertIn("created", resp_data)
        self.assertIn("modified", resp_data)
        self.assertEqual(resp_data["extra_fields"], {})
        self.assertEqual(resp_data["code"], currency_code)
        self.assertEqual(resp_data["name"], currency_name)
        self.assertEqual(resp_data["account"], self.account.id)

    def test__get_list_ok(self):
        self.create_test_currency()
        self.create_test_currency()

        response = self.client.get(self.url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__put_method_is_not_allowed(self):
        currency = self.create_test_currency()
        response = self.client.put(
            f"{self.url}{currency['id']}/",
            data=currency,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 405)

    @BaseTestCase.cases(
        ("new_name", {"name": "new_name"}),
        ("new_code", {"code": "new_"}),
    )
    def test__patch_ok(self, patch_data):
        currency = self.create_test_currency()
        response = self.client.patch(
            f"{self.url}{currency['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)

        response = self.client.get(
            f"{self.url}{currency['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        field = list(patch_data)[0]
        self.assertIn("new_", resp_data[field])

    def test__get_retrieve_ok(self):
        currency = self.create_test_currency()
        response = self.client.get(
            f"{self.url}{currency['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()

        self.assertEqual(resp_data["code"], currency["code"])
        self.assertEqual(resp_data["name"], currency["name"])
        self.assertEqual(resp_data["id"], currency["id"])

    def test__delete_ok(self):
        currency = self.create_test_currency()
        response = self.client.delete(
            f"{self.url}{currency['id']}/",
            data=None,
        )
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(Currency.objects.filter(id=currency["id"]).first())

    def test__list_with_name_filter(self):
        currency_1 = self.create_test_currency()
        currency_2 = self.create_test_currency()

        for currency in (currency_1, currency_2):
            url = f"{self.url}?filter.name={currency['name']}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(len(resp_data), 1)
            self.assertEqual(resp_data[0]["name"], currency["name"])

    def test__list_with_type_filter(self):
        _code = self.random_string(length=CODE_LENGTH)
        _ = self.create_test_currency(code=_code)
        _ = self.create_test_currency(code=_code)

        url = f"{self.url}?filter.code={_code}"
        response = self.client.get(url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__patch_extra_fields(self):
        account = self.create_test_currency()
        number = self.random_int(1000, 10000)
        string = self.random_string()
        future_date = str(BaseTestCase.random_future_date())
        patch_data = {
            "extra_fields": {
                "number": number,
                "string": string,
                "date": future_date,
            }
        }
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(resp_data["extra_fields"]["number"], number)
        self.assertEqual(resp_data["extra_fields"]["string"], string)
        self.assertEqual(resp_data["extra_fields"]["date"], future_date)

    def test__list_filter_by_extra_fields(self):
        account = self.create_test_currency()
        extra_fields = dict(
            number=self.random_int(1000, 10000),
            string=self.random_string(),
            date=str(BaseTestCase.random_future_date()),
        )
        patch_data = {"extra_fields": extra_fields}
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        for field, value in extra_fields.items():
            url = f"{self.url}?extra_fields.{field}={value}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(resp_data[0]["extra_fields"][field], value)
