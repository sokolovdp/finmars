from apps.common.tests.base_testcase import BaseTestCase
from apps.financial.models import Transaction
from apps.financial.settings import JSON_FORMAT, TYPE_LENGTH
from apps.financial.tests.factories import (
    AccountFactory,
    CurrencyFactory,
    InstrumentFactory,
    PortfolioFactory,
)

WRONG_TYPE = BaseTestCase.random_string(length=TYPE_LENGTH + 1)


# noinspection DuplicatedCode
class TransactionViewSetTest(BaseTestCase):
    def setUp(self):
        self.init_test_case()
        self.url = "/api/transaction/"
        self.account = AccountFactory(investor=self.investor)
        self.currency = CurrencyFactory(account=self.account)
        self.portfolio = PortfolioFactory(account=self.account)
        self.instrument = InstrumentFactory(
            account=self.account,
            currency=self.currency,
        )

    def create_test_transaction(
        self,
        _type=None,
        amount=None,
        price=None,
        currency=None,
        instrument=None,
        portfolio=None,
        account=None,
    ) -> dict:
        transaction_data = {
            "type": _type or self.random_string(),
            "amount": amount or self.random_decimal(),
            "price": price or self.random_decimal(),
            "currency": currency or self.currency.id,
            "instrument": instrument or self.instrument.id,
            "portfolio": portfolio or self.portfolio.id,
            "account": account or self.account.id,
        }
        response = self.client.post(self.url, data=transaction_data, format=JSON_FORMAT)
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        return resp_data

    @BaseTestCase.cases(
        ("no_data", {}),
        (
            "no_type",
            {
                "type": None,
                "amount": "10000.00",
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "no_amount",
            {
                "type": "type",
                "amount": None,
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "no_price",
            {
                "type": "type",
                "amount": "10000.00",
                "price": None,
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "no_account",
            {
                "type": "type",
                "amount": "10000.00",
                "price": "100.00",
                "account": None,
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "no_currency",
            {
                "type": "type",
                "amount": "10000.00",
                "price": "100.00",
                "account": 1,
                "currency": None,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "no_instrument",
            {
                "type": "type",
                "amount": "10000.00",
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": None,
                "portfolio": 1,
            },
        ),
        (
            "no_portfolio",
            {
                "type": "type",
                "amount": "10000.00",
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": None,
            },
        ),
    )
    def test__create_missing_data_error(self, transaction_data):
        response = self.client.post(self.url, data=transaction_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    @BaseTestCase.cases(
        (
            "bad_type",
            {
                "type": WRONG_TYPE,
                "amount": "10000.00",
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "bad_amount",
            {
                "type": "type",
                "amount": "100O",
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "bad_price",
            {
                "type": "type",
                "amount": "1000",
                "price": "10O.00",
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "bad_account",
            {
                "type": "type",
                "amount": "1000",
                "price": "100.00",
                "account": BaseTestCase.random_int(100, 10000),
                "currency": 1,
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "bad_currency",
            {
                "type": "type",
                "amount": "1000",
                "price": "100.00",
                "account": 1,
                "currency": BaseTestCase.random_int(100, 10000),
                "instrument": 1,
                "portfolio": 1,
            },
        ),
        (
            "bad_instrument",
            {
                "type": "type",
                "amount": "1000",
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": BaseTestCase.random_int(100, 10000),
                "portfolio": 1,
            },
        ),
        (
            "bad_portfolio",
            {
                "type": "type",
                "amount": "1000",
                "price": "100.00",
                "account": 1,
                "currency": 1,
                "instrument": 1,
                "portfolio": BaseTestCase.random_int(100, 10000),
            },
        ),
    )
    def test__create_invalid_data_error(self, invalid_data):
        response = self.client.post(self.url, data=invalid_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    def test__create_ok(self):
        transaction_type = self.random_string(length=TYPE_LENGTH)
        transaction_amount = str(self.random_decimal(10000, 10000000))
        transaction_price = str(self.random_decimal(10, 1000))
        transaction_data = {
            "type": transaction_type,
            "amount": transaction_amount,
            "price": transaction_price,
            "account": self.account.id,
            "currency": self.currency.id,
            "portfolio": self.portfolio.id,
            "instrument": self.instrument.id,
        }
        response = self.client.post(self.url, data=transaction_data, format=JSON_FORMAT)
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        self.assertIn("created", resp_data)
        self.assertIn("modified", resp_data)
        self.assertIn("notes", resp_data)
        self.assertEqual(resp_data["extra_fields"], {})
        self.assertEqual(resp_data["type"], transaction_type)
        self.assertEqual(resp_data["amount"], transaction_amount)
        self.assertEqual(resp_data["price"], transaction_price)
        self.assertEqual(resp_data["account"], self.account.id)
        self.assertEqual(resp_data["currency"], self.currency.id)
        self.assertEqual(resp_data["portfolio"], self.portfolio.id)
        self.assertEqual(resp_data["instrument"], self.instrument.id)

    def test__get_list_ok(self):
        self.create_test_transaction()
        self.create_test_transaction()

        response = self.client.get(self.url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__put_method_is_not_allowed(self):
        transaction = self.create_test_transaction()
        response = self.client.put(
            f"{self.url}{transaction['id']}/",
            data=transaction,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 405)

    @BaseTestCase.cases(
        ("new_type", "type", "new_type"),
        ("new_notes", "notes", "new_notes"),
        ("new_amount", "amount", str(BaseTestCase.random_decimal())),
        ("new_price", "price", str(BaseTestCase.random_decimal())),
    )
    def test__patch_ok(self, field, new_value):
        transaction = self.create_test_transaction()
        response = self.client.patch(
            f"{self.url}{transaction['id']}/",
            data={field: new_value},
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)

        response = self.client.get(
            f"{self.url}{transaction['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)
        self.assertEqual(resp_data[field], new_value)

    def test__get_retrieve_ok(self):
        transaction = self.create_test_transaction()
        response = self.client.get(
            f"{self.url}{transaction['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(resp_data["account"], self.account.id)
        self.assertEqual(resp_data["currency"], self.currency.id)
        self.assertEqual(resp_data["portfolio"], self.portfolio.id)
        self.assertEqual(resp_data["instrument"], self.instrument.id)

    def test__delete_ok(self):
        transaction = self.create_test_transaction()
        response = self.client.delete(
            f"{self.url}{transaction['id']}/",
            data=None,
        )
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(Transaction.objects.filter(id=transaction["id"]).first())

    def test__list_with_unique_type_filter(self):
        transaction_1 = self.create_test_transaction()
        transaction_2 = self.create_test_transaction()

        for transaction in (transaction_1, transaction_2):
            url = f"{self.url}?filter.type={transaction['type']}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(len(resp_data), 1)
            self.assertEqual(resp_data[0]["type"], transaction["type"])

    def test__list_with_common_type_filter(self):
        _type = self.random_string(length=TYPE_LENGTH)
        _ = self.create_test_transaction(_type=_type)
        _ = self.create_test_transaction(_type=_type)

        url = f"{self.url}?filter.type={_type}"
        response = self.client.get(url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__patch_extra_fields(self):
        account = self.create_test_transaction()
        number = self.random_int(1000, 10000)
        string = self.random_string()
        future_date = str(BaseTestCase.random_future_date())
        patch_data = {
            "extra_fields": {
                "number": number,
                "string": string,
                "date": future_date,
            }
        }
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(resp_data["extra_fields"]["number"], number)
        self.assertEqual(resp_data["extra_fields"]["string"], string)
        self.assertEqual(resp_data["extra_fields"]["date"], future_date)

    def test__list_filter_by_extra_fields(self):
        account = self.create_test_transaction()
        extra_fields = dict(
            number=self.random_int(1000, 10000),
            string=self.random_string(),
            date=str(BaseTestCase.random_future_date()),
        )
        patch_data = {"extra_fields": extra_fields}
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        for field, value in extra_fields.items():
            url = f"{self.url}?extra_fields.{field}={value}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(resp_data[0]["extra_fields"][field], value)
