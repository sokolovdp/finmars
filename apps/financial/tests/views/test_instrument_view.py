from datetime import date, datetime, timedelta
from decimal import Decimal

from apps.common.tests.base_testcase import BaseTestCase
from apps.financial.models import Instrument
from apps.financial.settings import ISIN_LENGTH, JSON_FORMAT, NAME_LENGTH
from apps.financial.tests.factories import AccountFactory, CurrencyFactory

WRONG_NAME = BaseTestCase.random_string(length=NAME_LENGTH + 1)
WRONG_ISIN = BaseTestCase.random_string(length=ISIN_LENGTH + 1)


# noinspection DuplicatedCode
class InstrumentViewSetTest(BaseTestCase):
    def setUp(self):
        self.init_test_case()
        self.url = "/api/instrument/"
        self.account = AccountFactory(
            name=self.random_string(),
            type=self.random_string(),
            investor=self.investor,
        )
        self.currency = CurrencyFactory(account=self.account)

    @staticmethod
    def today() -> date:
        return datetime.now().date()

    def tomorrow(self) -> date:
        return self.today() + timedelta(days=1)

    def create_test_instrument(
        self,
        name=None,
        isin=None,
        accrual_size=None,
        account=None,
        currency=None,
    ) -> dict:
        instrument_data = {
            "name": name or self.random_string(),
            "isin": isin or self.random_string(ISIN_LENGTH),
            "accrual_size": accrual_size or self.random_decimal(),
            "account": account or self.account.id,
            "currency": currency or self.currency.id,
        }
        response = self.client.post(self.url, data=instrument_data, format=JSON_FORMAT)
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        return resp_data

    @BaseTestCase.cases(
        ("no_data", {}),
        (
            "no_name",
            {
                "isin": "isin",
                "name": None,
                "accrual_size": "100.10",
                "account": 1,
                "currency": 1,
            },
        ),
        (
            "no_account",
            {
                "isin": "isin",
                "name": "name",
                "accrual_size": "100.10",
                "account": None,
                "currency": 1,
            },
        ),
        (
            "no_currency",
            {
                "isin": "isin",
                "name": "name",
                "accrual_size": "100.10",
                "account": 1,
                "currency": None,
            },
        ),
    )
    def test__create_missing_data_error(self, instrument_data):
        response = self.client.post(self.url, data=instrument_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    @BaseTestCase.cases(
        (
            "bad_isin",
            {
                "isin": WRONG_ISIN,
                "name": "name",
                "accrual_size": "100.10",
                "account": 1,
                "currency": 1,
            },
        ),
        (
            "bad_name",
            {
                "isin": "isin",
                "name": WRONG_NAME,
                "accrual_size": "100.10",
                "account": 1,
                "currency": 1,
            },
        ),
        (
            "bad_size",
            {
                "isin": "isin",
                "name": "name",
                "accrual_size": "qywtreyqt",
                "account": 1,
                "currency": 1,
            },
        ),
        (
            "bad_account",
            {
                "isin": "isin",
                "name": "name",
                "accrual_size": "100.10",
                "account": BaseTestCase.random_int(1000, 10000),
                "currency": 1,
            },
        ),
        (
            "bad_currency",
            {
                "isin": "isin",
                "name": "name",
                "accrual_size": "100.10",
                "account": 1,
                "currency": BaseTestCase.random_int(1000, 10000),
            },
        ),
    )
    def test__create_invalid_data_error(self, invalid_data):
        response = self.client.post(self.url, data=invalid_data, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 400)

    def test__create_ok(self):
        instrument_name = self.random_string(length=NAME_LENGTH)
        instrument_isin = self.random_string(length=ISIN_LENGTH)
        instrument_maturity_date = str(self.tomorrow())
        instrument_accrual_date = str(self.tomorrow())
        instrument_accrual_size = self.random_decimal(10000, 10000000)
        instrument_data = {
            "isin": instrument_isin,
            "name": instrument_name,
            "maturity_date": instrument_maturity_date,
            "accrual_date": instrument_accrual_date,
            "accrual_size": instrument_accrual_size,
            "account": self.account.id,
            "currency": self.currency.id,
        }
        response = self.client.post(self.url, data=instrument_data, format=JSON_FORMAT)
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        self.assertIn("created", resp_data)
        self.assertIn("modified", resp_data)
        self.assertIn("notes", resp_data)
        self.assertIn("description", resp_data)
        self.assertEqual(resp_data["extra_fields"], {})
        self.assertEqual(resp_data["isin"], instrument_isin)
        self.assertEqual(resp_data["name"], instrument_name)
        self.assertEqual(resp_data["account"], self.account.id)
        self.assertEqual(resp_data["currency"], self.currency.id)
        self.assertEqual(resp_data["maturity_date"], instrument_maturity_date)
        self.assertEqual(resp_data["accrual_date"], instrument_accrual_date)
        self.assertEqual(Decimal(resp_data["accrual_size"]), instrument_accrual_size)

    def test__get_list_ok(self):
        self.create_test_instrument()
        self.create_test_instrument()

        response = self.client.get(self.url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__put_method_is_not_allowed(self):
        instrument = self.create_test_instrument()
        response = self.client.put(
            f"{self.url}{instrument['id']}/",
            data=instrument,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 405)

    @BaseTestCase.cases(
        ("new_names", "name", "new_name"),
        ("new_notes", "notes", "new_notes"),
        ("new_isin", "isin", "new_isin"),
        ("new_description", "description", "new_description"),
        ("new_size", "accrual_size", str(BaseTestCase.random_decimal())),
        ("new_accrual_date", "accrual_date", str(BaseTestCase.random_future_date())),
        ("new_maturity_date", "maturity_date", str(BaseTestCase.random_future_date())),
    )
    def test__patch_ok(self, field, new_value):
        instrument = self.create_test_instrument()
        response = self.client.patch(
            f"{self.url}{instrument['id']}/",
            data={field: new_value},
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)

        response = self.client.get(
            f"{self.url}{instrument['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)
        self.assertEqual(resp_data[field], new_value)

    def test__get_retrieve_ok(self):
        instrument = self.create_test_instrument()
        response = self.client.get(
            f"{self.url}{instrument['id']}/",
            data=None,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(resp_data["account"], self.account.id)
        self.assertEqual(resp_data["currency"], self.currency.id)

    def test__delete_ok(self):
        instrument = self.create_test_instrument()
        response = self.client.delete(
            f"{self.url}{instrument['id']}/",
            data=None,
        )
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(Instrument.objects.filter(id=instrument["id"]).first())

    def test__list_with_unique_name_filter(self):
        instrument_1 = self.create_test_instrument()
        instrument_2 = self.create_test_instrument()

        for instrument in (instrument_1, instrument_2):
            url = f"{self.url}?filter.name={instrument['name']}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(len(resp_data), 1)
            self.assertEqual(resp_data[0]["name"], instrument["name"])

    def test__list_with_common_isin_filter(self):
        isin = self.random_string(length=ISIN_LENGTH)
        _ = self.create_test_instrument(isin=isin)
        _ = self.create_test_instrument(isin=isin)

        url = f"{self.url}?filter.isin={isin}"
        response = self.client.get(url, data=None, format=JSON_FORMAT)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__patch_extra_fields(self):
        account = self.create_test_instrument()
        number = self.random_int(1000, 10000)
        string = self.random_string()
        today = str(datetime.now().date())
        patch_data = {
            "extra_fields": {
                "number": number,
                "string": string,
                "date": today,
            }
        }
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(resp_data["extra_fields"]["number"], number)
        self.assertEqual(resp_data["extra_fields"]["string"], string)
        self.assertEqual(resp_data["extra_fields"]["date"], today)

    def test__list_filter_by_extra_fields(self):
        account = self.create_test_instrument()
        extra_fields = dict(
            number=self.random_int(1000, 10000),
            string=self.random_string(),
            today=str(datetime.now().date()),
        )
        patch_data = {"extra_fields": extra_fields}
        response = self.client.patch(
            f"{self.url}{account['id']}/",
            data=patch_data,
            format=JSON_FORMAT,
        )
        self.assertEqual(response.status_code, 200)
        for field, value in extra_fields.items():
            url = f"{self.url}?extra_fields.{field}={value}"
            response = self.client.get(url, data=None, format=JSON_FORMAT)
            self.assertEqual(response.status_code, 200)
            resp_data = response.json()
            self.assertEqual(resp_data[0]["extra_fields"][field], value)
