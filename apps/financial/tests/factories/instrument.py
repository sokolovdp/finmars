from datetime import datetime, timedelta

import factory
from factory.fuzzy import FuzzyDate, FuzzyDecimal, FuzzyText

from apps.financial.models import Instrument
from apps.financial.settings import ISIN_LENGTH, NAME_LENGTH
from apps.financial.tests.factories import AccountFactory, CurrencyFactory


class InstrumentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Instrument

    name = FuzzyText(length=NAME_LENGTH)
    isin = FuzzyText(length=ISIN_LENGTH)
    maturity_date = FuzzyDate(
        start_date=datetime.now().date() + timedelta(days=1),
        end_date=datetime.now().date() + timedelta(days=100),
    )
    accrual_date = FuzzyDate(
        start_date=datetime.now().date() + timedelta(days=1),
        end_date=datetime.now().date() + timedelta(days=100),
    )
    accrual_size = FuzzyDecimal(low=1, high=10000)
    account = factory.SubFactory(AccountFactory)
    currency = factory.SubFactory(CurrencyFactory)
