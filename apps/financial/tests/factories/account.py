import factory
from factory.fuzzy import FuzzyText

from apps.financial.models import Account
from apps.financial.settings import NAME_LENGTH, TYPE_LENGTH


class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account

    type = FuzzyText(length=TYPE_LENGTH)
    name = FuzzyText(length=NAME_LENGTH)
