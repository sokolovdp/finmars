import factory
from factory.fuzzy import FuzzyText

from apps.financial.models import Portfolio
from apps.financial.settings import NAME_LENGTH
from apps.financial.tests.factories.account import AccountFactory


class PortfolioFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Portfolio

    name = FuzzyText(length=NAME_LENGTH)
    account = factory.SubFactory(AccountFactory)
