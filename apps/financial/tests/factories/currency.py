import factory
from factory.fuzzy import FuzzyText

from apps.financial.models import Currency
from apps.financial.settings import CODE_LENGTH, NAME_LENGTH
from apps.financial.tests.factories.account import AccountFactory


class CurrencyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Currency

    code = FuzzyText(length=CODE_LENGTH)
    name = FuzzyText(length=NAME_LENGTH)
    account = factory.SubFactory(AccountFactory)
