from django.apps import AppConfig


class FinancialServiceConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.financial"
