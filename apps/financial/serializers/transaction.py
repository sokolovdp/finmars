from rest_framework.serializers import ModelSerializer

from apps.financial.models import Transaction

# from apps.financial.serializers import (
#     CurrencySerializer,
#     PortfolioSerializer,
#     InstrumentSerializer,
# )


class TransactionSerializer(ModelSerializer):
    class Meta:
        model = Transaction
        fields = [
            "id",
            "type",
            "amount",
            "price",
            "notes",
            "currency",
            "instrument",
            "portfolio",
            "account",
            "extra_fields",
            "created",
            "modified",
        ]
        read_only_fields = [
            "created",
            "modified",
        ]
