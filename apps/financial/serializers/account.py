from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.financial.models import Account
from apps.investor.models import Investor


class AccountSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        if not (
            investor := Investor.objects.filter(
                user=self.context["request"].user
            ).first()
        ):
            raise ValidationError("No such investor")

        attrs["investor"] = investor
        return attrs

    class Meta:
        model = Account
        fields = [  # do show to the user its internal investor id
            "id",
            "type",
            "name",
            "extra_fields",
            "created",
            "modified",
        ]
        read_only_fields = [
            "investor",
            "created",
            "modified",
        ]
