from rest_framework.serializers import ModelSerializer

from apps.financial.models import Instrument


class InstrumentSerializer(ModelSerializer):
    class Meta:
        model = Instrument
        fields = [
            "id",
            "name",
            "isin",
            "description",
            "maturity_date",
            "accrual_date",
            "accrual_size",
            "notes",
            "account",
            "currency",
            "extra_fields",
            "created",
            "modified",
        ]
        read_only_fields = [
            "created",
            "modified",
        ]
