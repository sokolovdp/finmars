from rest_framework.serializers import ModelSerializer

from apps.financial.models import Currency


class CurrencySerializer(ModelSerializer):
    class Meta:
        model = Currency
        fields = [
            "id",
            "code",
            "name",
            "account",
            "extra_fields",
            "created",
            "modified",
        ]
        read_only_fields = [
            "created",
            "modified",
        ]
