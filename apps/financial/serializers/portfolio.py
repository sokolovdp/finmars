from rest_framework.serializers import ModelSerializer

from apps.financial.models import Portfolio


class PortfolioSerializer(ModelSerializer):
    class Meta:
        model = Portfolio
        fields = [
            "id",
            "name",
            "strategy",
            "notes",
            "account",
            "extra_fields",
            "created",
            "modified",
        ]
        read_only_fields = [
            "created",
            "modified",
        ]
