from django.db import models

from apps.common.models import ExtraFieldsMixin, TimeMixin
from apps.financial.settings import (
    DEC_PLACES,
    DEFAULT_ORDER,
    ISIN_LENGTH,
    MAX_DIGITS,
    NAME_LENGTH,
)

RELATED_NAME = "instruments"


class Instrument(TimeMixin, ExtraFieldsMixin, models.Model):
    """
    Account instrument
    """

    name = models.CharField(
        max_length=NAME_LENGTH,
        db_index=True,
        help_text="instrument name",
    )
    isin = models.CharField(
        max_length=ISIN_LENGTH,
        db_index=True,
        null=True,
        blank=True,
        default=True,
        help_text="isin code",
    )
    description = models.TextField(
        null=True,
        blank=True,
        default=None,
        help_text="instrument description",
    )
    maturity_date = models.DateField(
        null=True,
        default=None,
        help_text="maturity date",
    )
    accrual_date = models.DateField(
        null=True,
        default=None,
        help_text="accrual date",
    )
    accrual_size = models.DecimalField(
        max_digits=MAX_DIGITS,
        decimal_places=DEC_PLACES,
        null=True,
        default=None,
        help_text="instrument amount",
    )
    notes = models.TextField(
        null=True,
        blank=True,
        default=None,
        help_text="user notes",
    )
    currency = models.ForeignKey(
        "Currency",
        related_name=RELATED_NAME,
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        "Account",
        related_name=RELATED_NAME,
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = [DEFAULT_ORDER]

    def __str__(self):
        return f"{self.account.id}-{self.id}-{self.isin}"
