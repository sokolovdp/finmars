from django.db import models

from apps.common.models import ExtraFieldsMixin, TimeMixin
from apps.financial.settings import DEC_PLACES, DEFAULT_ORDER, MAX_DIGITS, TYPE_LENGTH

RELATED_NAME = "transactions"


class TransactionManager(models.Manager):
    def get_queryset(self) -> models.query.QuerySet:
        return (
            super()
            .get_queryset()
            .select_related(
                "currency",
                "portfolio",
                "instrument",
            )
        )


class Transaction(TimeMixin, ExtraFieldsMixin, models.Model):
    """
    Account financial transaction
    """

    type = models.CharField(
        max_length=TYPE_LENGTH,
        db_index=True,
        help_text="transaction type",
    )
    amount = models.DecimalField(
        max_digits=MAX_DIGITS,
        decimal_places=DEC_PLACES,
        help_text="transaction amount",
    )
    price = models.DecimalField(
        max_digits=MAX_DIGITS,
        decimal_places=DEC_PLACES,
        help_text="transaction amount",
    )
    notes = models.TextField(
        null=True,
        blank=True,
        help_text="user notes",
    )
    currency = models.ForeignKey(
        "Currency",
        related_name=RELATED_NAME,
        on_delete=models.CASCADE,
    )
    instrument = models.ForeignKey(
        "Instrument",
        related_name=RELATED_NAME,
        on_delete=models.CASCADE,
    )
    portfolio = models.ForeignKey(
        "Portfolio",
        related_name=RELATED_NAME,
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        "Account",
        related_name=RELATED_NAME,
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = [DEFAULT_ORDER]

    def __str__(self):
        return f"{self.account.id}-{self.id}-{self.type}-{self.amount}"
