from django.db import models

from apps.common.models import ExtraFieldsMixin, TimeMixin
from apps.financial.settings import DEFAULT_ORDER, NAME_LENGTH, TYPE_LENGTH
from apps.investor.models import Investor


class Account(TimeMixin, ExtraFieldsMixin, models.Model):
    """
    User's account
    """

    type = models.CharField(
        max_length=TYPE_LENGTH,
        db_index=True,
        help_text="account type",
    )
    name = models.CharField(
        max_length=NAME_LENGTH,
        db_index=True,
        unique=True,
        help_text="account name",
    )
    investor = models.ForeignKey(
        Investor,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = [DEFAULT_ORDER]

    def __str__(self):
        return f"{self.investor.user.username}-{self.id}"
