from django.db import models

from apps.common.models import ExtraFieldsMixin, TimeMixin
from apps.financial.settings import DEFAULT_ORDER, NAME_LENGTH


class Portfolio(TimeMixin, ExtraFieldsMixin, models.Model):
    """
    Account portfolio
    """

    name = models.CharField(
        max_length=NAME_LENGTH,
        db_index=True,
        help_text="portfolio name",
    )
    strategy = models.TextField(
        null=True,
        blank=True,
        help_text="portfolio strategy",
    )
    notes = models.TextField(
        null=True,
        blank=True,
        help_text="portfolio notes",
    )
    account = models.ForeignKey(
        "Account",
        related_name="portfolios",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = [DEFAULT_ORDER]

    def __str__(self):
        return f"{self.account.id}-{self.id}-{self.name}"
