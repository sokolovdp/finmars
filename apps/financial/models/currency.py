from django.db import models

from apps.common.models import ExtraFieldsMixin, TimeMixin
from apps.financial.settings import CODE_LENGTH, DEFAULT_ORDER, NAME_LENGTH


class Currency(TimeMixin, ExtraFieldsMixin, models.Model):
    """
    Account currency
    """

    code = models.CharField(
        max_length=CODE_LENGTH,
        db_index=True,
        help_text="currency code",
    )
    name = models.CharField(
        max_length=NAME_LENGTH,
        db_index=True,
        help_text="currency name",
    )
    account = models.ForeignKey(
        "Account",
        related_name="currencies",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = [DEFAULT_ORDER]

    def __str__(self):
        return f"{self.account.id}-{self.id}-{self.code}"
