from django.contrib import admin

from apps.financial.models import Account, Currency, Instrument, Portfolio, Transaction

ITEMS_PER_PAGE = 20


class AccountAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = (
        "name",
        "type",
        "investor_name",
        "created",
        "modified",
    )

    @staticmethod
    def investor_name(obj):
        return str(obj.investor)


class PortfolioAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = (
        "name",
        "account",
    )


class CurrencyAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = (
        "code",
        "name",
        "account",
    )


class InstrumentAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = (
        "name",
        "isin",
        "maturity_date",
        "accrual_date",
    )


class TransactionAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = (
        "type",
        "created",
        "currency",
        "instrument",
    )


admin.site.register(Account, AccountAdmin)
admin.site.register(Portfolio, PortfolioAdmin)
admin.site.register(Currency, CurrencyAdmin)
admin.site.register(Instrument, InstrumentAdmin)
admin.site.register(Transaction, TransactionAdmin)
