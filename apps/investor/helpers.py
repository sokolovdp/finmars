import csv
import logging
from io import StringIO

from django.core.files import File
from rest_framework.exceptions import ValidationError

from apps.financial.models import Transaction
from apps.investor.models import Investor
from apps.investor.serializers import TransactionDataValidator

logger = logging.getLogger("common")
DELIMITER = ","
NEW_LINE = "\n"


def csv_file_parser(csv_file: File) -> list:
    """
    Convert csv file (in binary utf-8 encoding) to the list of transactions
    Sample result:
    [
        {
            "transaction": "buy",
            "instrument": "tsla",
            "amount": "10",
            "price": "100",
            "currency": "usd",
            "portfolio": "investor",
            "account": "1234-5123",
            "country": "USA",
            "asset_type": "stock",
            "sector": "it",
        },
        {
            "transaction": "sell",
            "instrument": "appl",
            "amount": "15",
            "price": "200",
            "currency": "eur",
            "portfolio": "risk",
            "account": "0923-5923",
            "country": "USA",
            "asset_type": "stock",
            "sector": "it",
        },
        ...
    ]
    """
    try:
        csv_str = "".join([line.decode() for line in csv_file.readlines()])
        transactions = list(
            csv.DictReader(
                StringIO(csv_str, newline=NEW_LINE),
                delimiter=DELIMITER,
            )
        )
    except Exception as e:
        message = f"csv_transaction_parser error: {repr(e)}"
        logger.error(message)
        raise ValidationError(message) from e

    return transactions


def create_transaction(investor: Investor, parsed_data: dict) -> Transaction:
    transaction_validator = TransactionDataValidator(data=parsed_data)
    transaction_validator.is_valid(raise_exception=True)
    return transaction_validator.create_transaction(investor=investor)
