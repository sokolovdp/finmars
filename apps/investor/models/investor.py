from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.db.models import signals
from rest_framework.authtoken.models import Token

from apps.common.models import TimeMixin
from apps.investor.settings import INVESTOR_NAME_LENGTH


class Investor(TimeMixin, models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name="investor",
        help_text="Django user",
    )
    vip = models.BooleanField(
        default=False,
        help_text="Vip investor flag",
    )
    name = models.CharField(
        max_length=INVESTOR_NAME_LENGTH,
        db_index=True,
        help_text="investor name",
    )

    class Meta:
        ordering = ["user"]

    def __str__(self):
        return self.user.email or self.user.username or f"investor:{self.user.id}"

    def generate_auth_token(self):
        token, _ = Token.objects.get_or_create(user=self.user)
        return token

    def send_email(
        self,
        subject: str,
        message: str,
        attachments: tuple = None,
        cc: list = None,
        bcc: list = None,
    ):
        """
        Example usage.

        investor.send_email(
            subject="Your latest financial report dates {datetime.now()}",
            message='Exported File is attached to this Message',
            attachments=(
                (
                    file_name,
                    output_buffer.getvalue(),
                    mimetypes.guess_type(file_name)[0]
                ),
            )
        )
        """
        if not self.user.email:
            return

        email = EmailMultiAlternatives(
            subject=subject,
            body=message,
            from_email=settings.EMAIL_FROM,
            to=[self.user.email],
            attachments=attachments,
            cc=cc,
            bcc=bcc,
        )
        email.send(fail_silently=False)


# noinspection PyUnusedLocal
def create_token(sender, instance=None, created=False, **kwargs):
    if created:
        instance.generate_auth_token()


signals.post_save.connect(create_token, sender=Investor)
