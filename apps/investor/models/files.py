from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models

from apps.common.models import TimeMixin
from apps.investor.settings import FILE_TITLE_LENGTH

file_storage = FileSystemStorage(location=settings.FILES_DIR)


class InvestorFile(TimeMixin, models.Model):
    title = models.CharField(
        max_length=FILE_TITLE_LENGTH,
        db_index=True,
        help_text="file title",
    )
    notes = models.TextField(
        blank=True,
        default="",
        help_text="investor notes",
    )
    file = models.FileField(
        storage=file_storage,
        help_text="investor file",
    )
    investor = models.ForeignKey(
        "Investor",
        related_name="files",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return f"{self.id}-{self.title}"
