from django.db.models import QuerySet
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_202_ACCEPTED
from rest_framework.viewsets import ModelViewSet

from apps.investor.helpers import create_transaction, csv_file_parser
from apps.investor.models import Investor, InvestorFile
from apps.investor.serializers import FileSerializer


# noinspection PyUnusedLocal
@extend_schema_view(
    create_transactions=extend_schema(
        request={"multipart/form-data": FileSerializer},
        responses={202: None},
    ),
)
@extend_schema(tags=["Investor Files"])
class FilesViewSet(ModelViewSet):
    """
    Investor Files CRUD API
    """

    permission_classes = [IsAuthenticated]
    pagination_class = None
    http_method_names = ["get", "post", "put", "delete"]
    queryset = InvestorFile.objects
    serializer_class = FileSerializer
    parser_classes = [MultiPartParser]
    investor = None

    def get_queryset(self) -> QuerySet:
        if not (investor := Investor.objects.filter(user=self.request.user).first()):
            raise ValidationError("No such investor")

        # ensure that investor can access only his files
        self.investor = investor
        return self.queryset.filter(investor=self.investor)

    @action(methods=["GET"], detail=True)
    def create_transactions(self, request, pk): #
        file_obj = self.get_object()
        transactions = csv_file_parser(csv_file=file_obj.file)
        for parsed_data in transactions:
            create_transaction(self.investor, parsed_data)
        return Response(status=HTTP_202_ACCEPTED)
