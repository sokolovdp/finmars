from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from apps.investor.models import Investor, InvestorFile

ITEMS_PER_PAGE = 20


class InvestorInline(admin.StackedInline):
    model = Investor
    can_delete = False
    verbose_name_plural = "investors"


class UserAdmin(BaseUserAdmin):
    inlines = (InvestorInline,)


class FilesAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = (
        "title",
        "investor_name",
        "created",
        "modified",
    )

    @staticmethod
    def investor_name(obj):
        return str(obj.investor)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(InvestorFile, FilesAdmin)
