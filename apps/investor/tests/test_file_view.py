from django.core.files.uploadedfile import SimpleUploadedFile

from apps.common.tests.base_testcase import BaseTestCase
from apps.financial.models import Account, Currency, Instrument, Portfolio, Transaction
from apps.investor.models import InvestorFile
from apps.investor.settings import FILE_TITLE_LENGTH

WRONG_TITLE = BaseTestCase.random_string(length=FILE_TITLE_LENGTH + 1)
MULTIPART = "multipart"
CSV_CONTENT = """transaction,instrument,amount,price,currency,portfolio,account,country,asset_type,sector
buy,tsla,10,100,usd,investor,1234-5123,USA,stock,it
sell,appl,15,200,eur,risk,0923-5923,USA,stock,it
sell,amzn,5,150,usd,investor,8881-2321,UK,stock,it
buy,twtr,2,75,chf,investor,4125-3912,USA,stock,it
"""
CSV_ENCODED = CSV_CONTENT.encode("UTF-8")


# noinspection DuplicatedCode
class FilesViewSetTest(BaseTestCase):
    def setUp(self):
        self.init_test_case()
        self.url = "/investor/files/"

    def create_test_file(self, title=None) -> dict:
        title = title or self.random_string()
        file_content = SimpleUploadedFile(
            name=f"{title}.csv",
            content=CSV_ENCODED,
        )
        file_data = {
            "title": title,
            "file": file_content,
        }
        response = self.client.post(
            self.url,
            data=file_data,
            request_format=MULTIPART,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        return resp_data

    @BaseTestCase.cases(
        ("no_data", {}),
        ("no_file", {"title": "title"}),
        ("no_name", {"file": CSV_ENCODED}),
    )
    def test__create_missing_data_error(self, account_data):
        response = self.client.post(self.url, data=account_data, format=MULTIPART)
        self.assertEqual(response.status_code, 400)

    @BaseTestCase.cases(
        ("bad_name", {"file": CSV_ENCODED, "title": WRONG_TITLE}),
    )
    def test__create_invalid_data_error(self, invalid_data):
        response = self.client.post(self.url, data=invalid_data, format=MULTIPART)
        self.assertEqual(response.status_code, 400)

    def test__create_ok(self):
        file_title = self.random_string(length=FILE_TITLE_LENGTH)
        file_notes = self.random_string()
        file_content = SimpleUploadedFile(
            name=file_title,
            content=CSV_ENCODED,
        )
        file_data = {
            "title": file_title,
            "notes": file_notes,
            "file": file_content,
        }
        response = self.client.post(
            self.url,
            data=file_data,
            request_format=MULTIPART,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 201, resp_data)
        self.assertIn("created", resp_data)
        self.assertIn("modified", resp_data)
        self.assertIn("file", resp_data)
        self.assertEqual(resp_data["notes"], file_notes)
        self.assertEqual(resp_data["title"], file_title)

    def test__get_list_ok(self):
        self.create_test_file()
        self.create_test_file()

        response = self.client.get(self.url, data=None, format=MULTIPART)
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()
        self.assertEqual(len(resp_data), 2)

    def test__put_method_is_not_allowed(self):
        file_data = self.create_test_file()
        response = self.client.patch(
            f"{self.url}{file_data['id']}/",
            data=file_data,
            format=MULTIPART,
        )
        self.assertEqual(response.status_code, 405)

    def test__get_retrieve_ok(self):
        file_data = self.create_test_file()
        response = self.client.get(
            f"{self.url}{file_data['id']}/",
            data=None,
            format=MULTIPART,
        )
        self.assertEqual(response.status_code, 200)
        resp_data = response.json()

        self.assertEqual(resp_data["title"], file_data["title"])
        self.assertEqual(resp_data["notes"], file_data["notes"])
        self.assertEqual(resp_data["id"], file_data["id"])

    def test__put_ok(self):
        old_file_data = self.create_test_file()
        new_title = self.random_string()
        new_notes = self.random_string()
        new_file_content = SimpleUploadedFile(
            name=f"{new_title}.csv",
            content=CSV_ENCODED,
        )
        new_file_data = {
            "title": new_title,
            "notes": new_notes,
            "file": new_file_content,
        }
        response = self.client.put(
            f"{self.url}{old_file_data['id']}/",
            data=new_file_data,
            format=MULTIPART,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)

        response = self.client.get(
            f"{self.url}{old_file_data['id']}/",
            data=None,
            format=MULTIPART,
        )
        resp_data = response.json()
        self.assertEqual(response.status_code, 200, resp_data)
        self.assertEqual(resp_data["title"], new_title)
        self.assertEqual(resp_data["notes"], new_notes)
        self.assertNotEqual(old_file_data["file"], resp_data["file"])

    def test__delete_ok(self):
        file_data = self.create_test_file()
        response = self.client.delete(
            f"{self.url}{file_data['id']}/",
            data=None,
        )
        self.assertEqual(response.status_code, 204)
        self.assertIsNone(InvestorFile.objects.filter(id=file_data["id"]).first())

    def test__create_transaction(self):
        file_data = self.create_test_file()
        response = self.client.get(
            f"{self.url}{file_data['id']}/create_transactions/",
            data=None,
            format=MULTIPART,
        )
        self.assertEqual(response.status_code, 202)

        # check that 4 different records were created according csv data
        self.assertEqual(Account.objects.all().count(), 4)
        self.assertEqual(Currency.objects.all().count(), 4)
        self.assertEqual(Portfolio.objects.all().count(), 4)
        self.assertEqual(Instrument.objects.all().count(), 4)
        self.assertEqual(Transaction.objects.all().count(), 4)
