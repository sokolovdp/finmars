from django.db import DatabaseError, transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.financial.models import Account, Currency, Instrument, Portfolio, Transaction
from apps.financial.settings import DEC_PLACES, MAX_DIGITS
from apps.investor.models import Investor, InvestorFile


class FileSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        if not (
            investor := Investor.objects.filter(
                user=self.context["request"].user
            ).first()
        ):
            raise ValidationError("No such investor")

        attrs["investor"] = investor
        return attrs

    class Meta:
        model = InvestorFile
        fields = [
            "id",
            "title",
            "notes",
            "file",
            "created",
            "modified",
        ]
        read_only_fields = [
            "created",
            "modified",
        ]


# noinspection PyAbstractClass
class TransactionDataValidator(serializers.Serializer):
    transaction = serializers.CharField()
    amount = serializers.DecimalField(max_digits=MAX_DIGITS, decimal_places=DEC_PLACES)
    price = serializers.DecimalField(max_digits=MAX_DIGITS, decimal_places=DEC_PLACES)
    country = serializers.CharField()
    asset_type = serializers.CharField()
    sector = serializers.CharField()
    account = serializers.CharField()
    currency = serializers.CharField()
    portfolio = serializers.CharField()
    instrument = serializers.CharField()

    def _create_transaction_with_components(self, investor: Investor) -> Transaction:
        account_data = {
            "investor": investor,
            "name": self.validated_data["account"],
            "type": self.validated_data["asset_type"],
        }
        account, _ = Account.objects.update_or_create(**account_data)
        currency_data = {
            "account": account,
            "code": self.validated_data["currency"],
            "name": self.validated_data["country"],
        }
        currency, _ = Currency.objects.update_or_create(**currency_data)
        portfolio_data = {
            "account": account,
            "name": self.validated_data["portfolio"],
        }
        portfolio, _ = Portfolio.objects.update_or_create(**portfolio_data)
        instrument_data = {
            "account": account,
            "currency": currency,
            "name": self.validated_data["instrument"],
        }
        instrument, _ = Instrument.objects.update_or_create(**instrument_data)
        transaction_data = {
            "account": account,
            "currency": currency,
            "portfolio": portfolio,
            "instrument": instrument,
            "type": self.validated_data["transaction"],
            "amount": self.validated_data["amount"],
            "price": self.validated_data["price"],
        }
        return Transaction.objects.update_or_create(**transaction_data)

    def create_transaction(self, investor: Investor) -> Transaction:
        try:
            with transaction.atomic():
                return self._create_transaction_with_components(investor)
        except DatabaseError as e:
            raise ValidationError(f"{repr(e)}") from e
