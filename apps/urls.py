"""
    Finmars URL Configuration
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework import routers

from apps.common.views import VersionView
from apps.financial.views import (
    AccountViewSet,
    CurrencyViewSet,
    InstrumentViewSet,
    PortfolioViewSet,
    TransactionViewSet,
)
from apps.investor.views import FilesViewSet

admin.autodiscover()

API_TITLE = "FinMars Api"
API_DESCRIPTION = "Brand new financial service API from FinMars"

api_router = routers.DefaultRouter()
api_router.register("account", AccountViewSet, basename="account")
api_router.register("currency", CurrencyViewSet, basename="currency")
api_router.register("transaction", TransactionViewSet, basename="transaction")
api_router.register("portfolio", PortfolioViewSet, basename="portfolio")
api_router.register("instrument", InstrumentViewSet, basename="instrument")

investor_router = routers.DefaultRouter()
investor_router.register("files", FilesViewSet, basename="files")

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(api_router.urls)),
    path("investor/", include(investor_router.urls)),
    path(
        "version/",
        VersionView.as_view({"get": "retrieve"}),
        name="version",
    ),
]

if settings.DEBUG:

    urlpatterns.append(
        path(
            "",
            include("rest_framework.urls", namespace="rest_framework"),
        ),
    )
    urlpatterns.append(
        path(
            "__debug__/",
            include("debug_toolbar.urls"),
        ),
    )
    urlpatterns.append(
        path(
            "docs/",
            SpectacularSwaggerView.as_view(url_name="schema"),
            name="swagger-ui",
        )
    )
    urlpatterns.append(
        path(
            "schema/",
            SpectacularAPIView.as_view(),
            name="schema",
        ),
    )
