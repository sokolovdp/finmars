# Finmars demo project

## About
This project was developed for demo purposes according to provided requirements using Python 3.11, Django 4.1, DRF 3.14 and default SQLite3 2.6 Database, with assistance of the built-in CI/CD in GitLab.

## GitLab repository
Project source code is located at Gitlab site in the public repository https://gitlab.com/sokolovdp/finmars.git

## Installation

- clone source code using git from Gitlab repository
- in the root folder ./finmars create folder _**vars**_
```
mkdir vars
```
- in the root folder ./finmars create virtual python environment
```
python3 -m venv venv
```
- activate virtual environment, and install project requirements
```
source venv/bin/activate
pip install -r dev_requirements.txt
```

## Run linters checks & tests with coverage report

- in the active virtual environment run the following commands:
```
black ./apps
flake8 ./apps
coverage run --rcfile=.coverage_rc manage.py test --parallel=4
coverage combine && coverage report
```
- you should see the following test report
```
TOTAL              1362     39    152     14    96%
```

## Run server with **_OpenAPI 3.0_** page

- in the active virtual environment run the following commands:
```
python manage.py migrate
python manage.py runserver 8000
```

- you should see the following text:
```
...
Django version 4.1.5, using settings 'apps.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```
- in the browser you can open the page with OpenAPI description of all available project's APIs: 
http://127.0.0.1:8000/docs/

## Access to Django Admin page

- to get access to Django Admin page you have to create superuser. For that in the active virtual environment run the following command, and provide username, email and password:
```
python manage.py createsuperuser
```
- now in the browser at the login page you can provide superuser credentials, to get access to all Django admin views & tools: 
http://127.0.0.1:8000/admin/

- while in Django Admin you can generate API access token by adding Investor to the Django user in the Users table, then token automatically appears in Tokens table.

## Further developments 
The following steps are under **_development_**:
- migration to PostgreSQL 15.1 database engine
- project containerization based on Docker containers
